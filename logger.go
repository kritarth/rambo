package main

import (
	"fmt"
	"github.com/kataras/golog"
	"runtime"
	"path/filepath"
)

func GetLogger(id string, callerSkip int) *golog.Logger {
	newLogger := golog.New()
	newLogger.SetLevel("debug")
	newLogger.SetTimeFormat("2006-01-02 15:04:05.999999")
	newLogger.Handle(func(l *golog.Log) bool {
		prefix := golog.GetTextForLevel(l.Level, true)
		pc, fn, line, _ := runtime.Caller(callerSkip)
		message := fmt.Sprintf("%s %s %s %s:%d %s: %s",
			prefix, id, runtime.FuncForPC(pc).Name(),filepath.Base(fn), line, l.FormatTime(), l.Message)

		if l.NewLine {
			message += "\n"
		}

		fmt.Print(message)
		return true
	})
	return newLogger
}
