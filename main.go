package main

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/gocraft/work"
	"net/http"
	"os"
	"fmt"
	"time"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"strings"
	"encoding/json"
	"sync"
)

type Context struct {}

var recordingStatus sync.Map
var enqueuer = work.NewEnqueuer("radeus", redisPool)
//var callbackEnqueuer = work.NewEnqueuer("callback", callbackRedisPool)
var log = GetLogger("radeus", 6)

var s3Bucket = fmt.Sprintf("plivo-recordings-dev")
var sess = session.New((&aws.Config{
	Region: aws.String(os.Getenv("S3_REGION")),
}))
var s3client = s3.New(sess)

func DownloadServerInit(router chi.Router) {
	var callRouter = chi.NewRouter()
	callRouter.Get("/", ServeFile)
	router.Mount("/v1/file", callRouter)
}

type ProcessFileData struct {
	FileName string `json:"file_name"`
}

func ProcessFile(w http.ResponseWriter, r *http.Request) {
	var fileData ProcessFileData
	json.NewDecoder(r.Body).Decode(&fileData)
	fileData.FileName = strings.Replace(fileData.FileName, "/", "_", -1)
	log.Info(fileData.FileName, " process file request received")
	_, err := enqueuer.EnqueueUnique("download", work.Q{"file_name": fileData.FileName})
	if err != nil {
		log.Error(fileData.FileName, " enqueue failed\nerr: ", err)
		w.WriteHeader(503)
	}
}

func ServeFile(w http.ResponseWriter, r *http.Request) {
	authId := chi.URLParam(r, "authId")
	fileNameType := strings.Split(fmt.Sprintf("%s_%s", authId, chi.URLParam(r, "fileName")), ".")
	fileName := fileNameType[0]
	log.Info(fileName, " download request received")
	w.Header().Add("Cache-Control", "no-store")

	switch fileNameType[1] {
	case "wav":
		// forward to s3 url
		log.Info(fileName, " forward to s3")
		//http.Redirect(w, r, fmt.Sprintf("http://localhost:8080/v1/%s", fileName), http.StatusSeeOther)
		break
	case "mp3":
		// estimate eta
		// query the encode workers
		// respond with the eta
		where, ok := recordingStatus.Load(fileName)
		if ok {
			log.Info(fileName, " streaming from temporary file")
			switch where {
			case "upload":
				// just wait until upload is finished
				for {
					if !ok || where != "upload" {
						break
					}
					time.Sleep(time.Second)
					where, ok = recordingStatus.Load(fileName)
				}
				// forward to s3
				//http.Redirect(w, r, fmt.Sprintf("http://localhost:8080/v1/%s", fileName), http.StatusSeeOther)
				return
			case "download":
				// wait till encoding starts because thats when the temporary file is created
				for {
					if !ok {
						// forward to s3
						return
					}
					if where != "download" {
						break
					}
					time.Sleep(time.Second)
					where, ok = recordingStatus.Load(fileName)
				}
				break
			}
			//w.Write([]byte("file not found, eta is 5 minutes."))
			w.Header().Add("Content-Type", "audio/mpeg")
			w.Header().Add("Content-Disposition", fmt.Sprintf("inline; filename=%s", fileName))
			readFileUntilDone(fileName, w)
		} else {
			// forward to s3
			log.Info(fileName, " forward to s3")
			//http.Redirect(w, r, fmt.Sprintf("http://localhost:8080/v1/%s", fileName), http.StatusSeeOther)
		}
	}
}
func Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(
		render.SetContentType(render.ContentTypeJSON), // Set content-Type headers as application/json
		middleware.Logger,                             // Log API request calls
		middleware.DefaultCompress,                    // Compress results, mostly gzipping assets and json
		middleware.RedirectSlashes,                    // Redirect slashes to no slash URL versions
		middleware.Recoverer,                          // Recover from panics without crashing server
	)

	router.Get("/v1/file/{authId}/{fileName}", ServeFile)
	router.Post("/v1/process", ProcessFile)

	DownloadServerInit(router)

	return router
}

func main() {
	router := Routes()

	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Infof("%s %s\n", method, route) // Walk and print out all routes
		return nil
	}
	if err := chi.Walk(router, walkFunc); err != nil {
		log.Fatalf("Logging err: %s\n", err.Error()) // panic if there is an error
	}

	go StartDownloader()
	go StartEncoder()
	go StartUploader()
	go StartReporter()
	log.Fatal(http.ListenAndServe(":8080", router)) // Note, the port is usually gotten from the environment.
}

