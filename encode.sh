input_file=${1}.wav
output_file=${2}.mp3
size=`stat -c%s "${input_file}"`
if [[ size -lt 5120000 ]]; then
    # small size do not split
    lame ${input_file} ${output_file} > /dev/null 2>&1
    exit $?;
fi
ffmpeg -i ${input_file} -f segment -segment_time 300 -c copy ${input_file}.%03d.wav > /dev/null 2>&1
list=`ls ${input_file}.* |sort`

concat=""
for f in `ls ${input_file}.* |sort`; do
    lame $f $f.mp3 > /dev/null 2>&1 &
    concat=${concat}$f.mp3' '
done
wait

mp3wrap ${output_file} ${concat} &&
    mv ${2}_MP3WRAP.mp3 ${output_file} &&
    rm ${concat} &&
    rm ${input_file}.*
