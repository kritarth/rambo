package main

import (
	"os"
	"io"
	"time"
	"fmt"
	"net/http"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func readFileUntilDone(fileName string, w http.ResponseWriter) {
	exists := false
	for i := 0; i < 5; i++ {
		if fileExists(fmt.Sprintf("/tmp/mp3/%s.mp3", fileName)) {
			exists = true
			break
		}
		time.Sleep(time.Second)
	}
	if !exists {
		log.Error(fileName, " was supposed to be found")
		return
	}
	f, err := os.Open(fmt.Sprintf("/tmp/mp3/%s.mp3", fileName))
	if err != nil {
		log.Error(fileName, " error opening file\nerr: ", err)
		return
	}
	defer f.Close()
	info, err := f.Stat()
	if err != nil {
		log.Error(fileName, " error getting file info\nerr: ", err)
		return
	}
	oldSize := info.Size()
	// break if no change for 3 seconds
	noChange := 3
	for {
		_, err := io.Copy(w, f)
		if err != nil {
			log.Error(fileName, " error copying file\nerr: ", err)
			return
		}
		pos, err := f.Seek(0, io.SeekCurrent)
		if err != nil {
			log.Error(fileName, " error seeking file\nerr: ", err)
			return
		}
		for {
			time.Sleep(time.Second)
			newinfo, err := f.Stat()
			if err != nil {
				log.Error(fileName, " error getting file info\nerr: ", err)
				return
			}
			newSize := newinfo.Size()
			if newSize != oldSize {
				log.Debug("Change in file, ", oldSize, " -> ", newSize)
				if newSize < oldSize {
					f.Seek(0, io.SeekStart)
				} else {
					f.Seek(pos, io.SeekStart)
				}
				oldSize = newSize
				noChange = 3
				break
			}
			log.Info("No change ", noChange)
			_, ok := recordingStatus.Load(fileName)
			if !ok {
				noChange -= 1
			}
			if !ok && noChange == 0 {
				log.Debug("No change in file for 3 seconds. Returning.")
				break
			}
		}
		_, ok := recordingStatus.Load(fileName)
		if !ok && noChange == 0 {
			break
		}
	}
}
