package main

import (
	"github.com/gocraft/work"
	"os/exec"
	"fmt"
	"bytes"
	"io"
	"os"
)

var encodeLog = GetLogger("encoder", 5)
var encodePool = work.NewWorkerPool(Context{}, 6, "radeus", redisPool)

func Encode(job *work.Job) error {
	fileName := job.ArgString("file_name")
	recordingStatus.Store(fileName, "encode")

	cmd := exec.Command(
		"bash",
		"-c",
		fmt.Sprintf(
			"/home/kritarth/rambo/encode.sh /tmp/wav/%s /tmp/mp3/%s",
			fileName,
			fileName,
		),
	)
	stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()
	err := cmd.Start()
	if err != nil {
		encodeLog.Error("cmd.Start() failed with ", err)
		return err
	}

	var stdoutBuf bytes.Buffer
	var stderrBuf bytes.Buffer
	go func() {
		io.Copy(&stdoutBuf, stdoutIn)
	}()

	go func() {
		io.Copy(&stderrBuf, stderrIn)
	}()

	err = cmd.Wait()
	if err != nil {
		encodeLog.Error("cmd.Run() failed with ", err)
		return err
	}
	encodeLog.Info(fileName, " encode success\nstdout: ", string(stdoutBuf.Bytes()), "\nstderr: ", string(stderrBuf.Bytes()))

	_, err = enqueuer.EnqueueUnique("upload", work.Q{"file_name": fileName})

	// remove wav file
	err = os.Remove(fmt.Sprintf("/tmp/wav/%s.wav", fileName))
	if err != nil {
		uploadLog.Error(fileName, " wav file delete failure\nerr: ", err)
		return nil
	}
	return err
}
func StartEncoder() {
	// Customize options:
	encodePool.JobWithOptions("encode", work.JobOptions{Priority: 10, MaxFails: 1}, Encode)

	// Start processing jobs
	encodePool.Start()
}
