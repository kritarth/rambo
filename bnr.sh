#!/bin/bash
mkdir /tmp/wav
mkdir /tmp/mp3
echo "building" &&
    export S3_REGION="us-east-1" &&
    export PATH=$PATH:/usr/local/go/bin &&
    go build -o radeus *.go &&
    echo "running" &&
    ./radeus |tee radeus.log
