#!/bin/bash
mkdir /tmp/wav
mkdir /tmp/mp3
IFS=$'\n'
for i in `cat s3_load_data`; do
    sleep `python -c "print 1.0/$1"` &&
    echo "$i" &&
    uuid=`echo $i |cut -d' ' -f2`;
    curl -X POST -d '{"file_name": "'${uuid}'"}' http://localhost:8080/v1/process &> /dev/null &
done
unset IFS
wait
#watch -n 2 "aws s3 ls s3://dev-voice-lambda-triggers/"
