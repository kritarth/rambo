#!/bin/bash

if ((${EUID:-0} || "$(id -u)")); then
    echo You are not root.
    exit 1
else
    echo Hello, root.
fi

apt install redis-server

cd /tmp
wget https://dl.google.com/go/go1.12.6.linux-amd64.tar.gz
tar xvf go1.12.6.linux-amd64.tar.gz
chown -R root:root ./go
mv go /usr/local
echo "export GOPATH=$HOME/work" >> ~/.profile
echo "export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin" >> ~/.profile
source ~/.profile
