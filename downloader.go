package main

import (
	"github.com/gocraft/work"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"os"
	"fmt"
	"strings"
)

var downloadLog = GetLogger("downloader", 5)
var downloadPool = work.NewWorkerPool(Context{}, 2, "radeus", redisPool)
// Create an downloader with the session and default options
var s3Downloader = s3manager.NewDownloader(sess)

func Download(job *work.Job) error {
	fileName := job.ArgString("file_name")
	recordingStatus.Store(fileName, "download")

	file, err := os.OpenFile(fmt.Sprintf("/tmp/wav/%s.wav", fileName), os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		downloadLog.Error(fileName, " error opening file\nerr: ", err)
	}

	// Perform a download.
	downloadLog.Info(fileName, " download start")
	result, err := s3Downloader.Download(
		file,
		&s3.GetObjectInput {
			Bucket: aws.String(s3Bucket),
			Key: aws.String(fmt.Sprintf("%s.wav", strings.Replace(fileName, "_", "/", -1))),
		},
	)
	if err != nil {
		downloadLog.Error(fileName, " error downloading file\nerr: ", err)
		return err
	}
	downloadLog.Info(fileName, " download success\nresult: ", result)

	// add encoder task
	_, err = enqueuer.EnqueueUnique("encode", work.Q{"file_name": fileName})

	return err
}
func StartDownloader() {
	// Customize options:
	downloadPool.JobWithOptions("download", work.JobOptions{Priority: 10, MaxFails: 1}, Download)

	// Start processing jobs
	downloadPool.Start()
}
