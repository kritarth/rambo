module radeus

go 1.12

require (
	github.com/aws/aws-sdk-go v1.20.12
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/gocraft/work v0.5.1
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/kataras/golog v0.0.0-20190624001437-99c81de45f40
	github.com/kataras/pio v0.0.0-20190103105442-ea782b38602d // indirect
	github.com/robfig/cron v1.2.0 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
)
