package main

import (
	"time"
)

var reportLog = GetLogger("reporter", 5)

func StartReporter() {
	for {
		time.Sleep(time.Duration(5) * time.Second)
		recordingStatus.Range(func(key interface{}, value interface{}) bool {
			reportLog.Debug(key.(string), "=", value.(string))
			return true
		})
	}
}
