package main

import (
	"github.com/gomodule/redigo/redis"
	"github.com/gocraft/work"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"os"
	"fmt"
	"strings"
)

var uploadLog = GetLogger("uploader", 5)
var redisPool = &redis.Pool{
	MaxActive: 10,
	MaxIdle: 10,
	Wait: true,
	Dial: func() (redis.Conn, error) {
		return redis.Dial("tcp", ":6379")
	},
}
var uploadPool = work.NewWorkerPool(Context{}, 2, "radeus", redisPool)

// Create an uploader with the session and default options
var s3Uploader = s3manager.NewUploader(sess)

func Upload(job *work.Job) error {
	fileName := job.ArgString("file_name")
	recordingStatus.Store(fileName, "upload")

	file, err := os.Open(fmt.Sprintf("/tmp/mp3/%s.mp3", fileName))
	if err != nil {
		uploadLog.Error(fileName, " error opening file\nerr: ", err)
	}
	// Upload input parameters
	upParams := &s3manager.UploadInput{
		Bucket: &s3Bucket,
		Key:    aws.String(fmt.Sprintf("%s.mp3", strings.Replace(fileName, "_", "/", -1))),
		Body:   file,
	}

	// Perform an upload.
	result, err := s3Uploader.Upload(upParams)
	if err != nil {
		uploadLog.Error(fileName, " error uploading file\nerr: ", err)
		return err
	}
	uploadLog.Info(fileName, " upload success\nresult: ", result)
	recordingStatus.Delete(fileName)
	/*// remove mp3 file
	err = os.Remove(fmt.Sprintf("/tmp/mp3/%s.mp3", fileName))
	if err != nil {
		uploadLog.Error(fileName, " mp3 file delete failure\nerr: ", err)
		return nil
	}*/
	return err
}
func StartUploader() {
	// Customize options:
	uploadPool.JobWithOptions("upload", work.JobOptions{Priority: 10, MaxFails: 1}, Upload)

	// Start processing jobs
	uploadPool.Start()
}
