#!/bin/bash

mkdir /tmp/wav
mkdir /tmp/mp3
uuid=`uuidgen`
file=`cat 9_10_data |shuf -n1 |xargs -n1 -I {} /bin/bash -c "echo beethoven_first_{}_seconds.wav"`
# create wav in s3
aws s3 cp s3://dev-voice-lambda-triggers/${file} s3://plivo-recordings-dev/${uuid}.wav &&
    # hit the process url
    curl -X POST -d '{"file_name": "'${uuid}'"}' http://localhost:8080/v1/process

# list the mp3 in s3
#watch -n 2 "aws s3 ls s3://dev-voice-lambda-triggers/${uuid}.mp3"
