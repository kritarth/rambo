#!/bin/bash
rm s3_load_data
for i in `seq 1 $1`; do
    file=`cat 9_10_data |shuf -n1 |xargs -n1 -I {} /bin/bash -c "echo beethoven_first_{}_seconds.wav"`
    uuid=`uuidgen`
    # create wav in s3
    aws s3 cp s3://dev-voice-lambda-triggers/${file} s3://plivo-recordings-dev/${uuid}.wav &
    echo "${file} ${uuid}" >> s3_load_data
done
wait
echo "data prepared"
